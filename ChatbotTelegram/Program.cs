﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.InputFiles;

namespace ChatbotTelegram
{
    class Program
    {
        static TelegramBotClient Bot = new TelegramBotClient("2103579675:AAHr2QKbRfUINOQ-ffNU4GOIzYxL1PKeRaY");
        static string hinhanh = @"D:\Images\images\";
        static List<string> anh = new List<string>
        {
            "mèo.jfif",
            "hoa.jpg",
            "cho.jpg",
            "video.mp4"

        };

        static void Main(string[] args)
        {
            Bot.StartReceiving();
            Bot.OnMessage += Bot_OnMessage;
            Console.ReadLine();
        }

        private static async void Bot_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {

            var text = e.Message.Text;
            text = text.ToUpper();
            switch (text)
            {
                case "NGÀY GIỜ HÔM NAY":
                    var ngay = DateTime.Now.ToString();
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, ngay);
                    break;
                case "TÊN BẠN LÀ GÌ":
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "Telegram Bot Nhóm 21");
                    break;
                case "HELLO":
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "Hello " + e.Message.Chat.Username);
                    break;
                case "MÈO":
                    using (var stream = File.OpenRead(hinhanh + anh[0]))
                    {
                        InputOnlineFile nhapanh = new InputOnlineFile(stream);
                        await Bot.SendPhotoAsync(e.Message.Chat.Id, nhapanh);
                        Bot.SendTextMessageAsync(e.Message.Chat.Id, "Đây là mèo của tôi");
                    }
                    break;
                case "CHÓ":
                    using (var stream = File.OpenRead(hinhanh + anh[2]))
                    {
                        InputOnlineFile nhapanh = new InputOnlineFile(stream);
                        await Bot.SendPhotoAsync(e.Message.Chat.Id, nhapanh);
                        Bot.SendTextMessageAsync(e.Message.Chat.Id, "Đây là chó của tôi");
                    }
                    break;
                case "HOA":
                    using (var stream = File.OpenRead(hinhanh + anh[1]))
                    {
                        InputOnlineFile nhapanh = new InputOnlineFile(stream);
                        await Bot.SendPhotoAsync(e.Message.Chat.Id, nhapanh);
                        Bot.SendTextMessageAsync(e.Message.Chat.Id, "Đây là hoa tôi tìm được");
                    }
                    break;
                case "VIDEO":
                    using (var stream = File.OpenRead(hinhanh + anh[3]))
                    {
                        InputOnlineFile nhapanh = new InputOnlineFile(stream);
                        await Bot.SendVideoAsync(e.Message.Chat.Id, nhapanh);
                     
                    }
                    break;
                default:
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, "Xin lỗi tôi không hiểu");
                    break;

            }

        }
    }
}
    

